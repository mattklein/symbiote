// Copyright 2017, the project authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE.md file.

package symbiote

import (
	"testing"

	"bitbucket.org/mattklein/symbiote/networking"
	"bitbucket.org/mattklein/symbiote/platform"
)

const (
	TestHost = "127.0.0.1"
	TestPort = 7337
)

func TestNewDriver(t *testing.T) {
	// start a server
	server, err := networking.Serve(TestHost, TestPort)
	if err != nil {
		t.Error(err)
	}
	defer server.Close()

	// connect with the client
	client, err := networking.Connect(TestHost, TestPort)
	if err != nil {
		t.Error(err)
	}
	defer client.Close()

	// build the windowing system
	system, err := platform.NewWaylandWindowSystem()
	if err != nil {
		t.Error(err)
	}

	// build the driver
	NewDriver(client, system)
}
