// Copyright 2017, the project authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE.md file.

package utils

import "testing"

func TestAtomicBool(t *testing.T) {
	value := AtomicFalse
	if value.IsTrue() {
		t.Fail()
	}

	value.SetTrue()
	if !value.IsTrue() {
		t.Fail()
	}

	value.SetFalse()
	if value.IsTrue() {
		t.Fail()
	}
}
