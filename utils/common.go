// Copyright 2017, the project authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE.md file.

package utils

import "sync/atomic"

// Atomic boolean value based on an atomic integer.
type AtomicBool int32

// Common atomic boolean values.
const (
	AtomicTrue  AtomicBool = 1
	AtomicFalse AtomicBool = 0
)

func (b *AtomicBool) IsTrue() bool {
	return atomic.LoadInt32((*int32)(b)) != 0
}

func (b *AtomicBool) SetTrue() {
	atomic.StoreInt32((*int32)(b), 1)
}

func (b *AtomicBool) SetFalse() {
	atomic.StoreInt32((*int32)(b), 0)
}
