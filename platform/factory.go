// Copyright 2017, the project authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE.md file.

package platform

import "errors"

// The window systems supported by the implementations.
var factories = map[string]func() (WindowSystem, error){
	"macos": func() (WindowSystem, error) {
		panic("Not yet implemented")
	},
	"wayland": func() (WindowSystem, error) {
		return NewWaylandWindowSystem()
	},
	"windows": func() (WindowSystem, error) {
		panic("Not yet implemented")
	},
	"xorg": func() (WindowSystem, error) {
		panic("Not yet implemented")
	},
}

// Builds a window system with the given name.
func BuildSystem(name string) (WindowSystem, error) {
	if factory, ok := factories[name]; ok {
		return factory()
	}
	return nil, errors.New("The system " + name + " is unsupported")
}

// Validates that the given system name is valid.
func IsSupported(name string) bool {
	if _, ok := factories[name]; ok {
		return true
	}
	return false
}

// Selects a default system to use.
func SelectDefault() string {
	return "wayland" // TODO: determine this somehow
}
