// Copyright 2017, the project authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE.md file.

package platform

// Represents abstractly a windowing system that is implemented by some desktop manager.
type WindowSystem interface {
	PressKey(key uint32)        // Presses and releases the given key.
	MovePointer(from, to Point) // Moves the mouse pointer from the given point, to the given point.
	SetClipboard(raw string)    // Places the given value into the clipboard.
}

// Represents a 2d point.
type Point struct {
	X, Y int32
}
