// Copyright 2017, the project authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE.md file.

package platform

import "github.com/stanluk/wayland-client"

// Supports the Wayland UI display management architecture on newer Linux distributions.
type WaylandWindowSystem struct {
	display    *wayland.Display
	connection *wayland.Connection
	keyboard   *wayland.Keyboard
	pointer    *wayland.Pointer
}

// Creates a new Wayland-based window system.
func NewWaylandWindowSystem() (*WaylandWindowSystem, error) {
	const DefaultDisplay = "" // empty addr => find the default display
	display, err := wayland.ConnectDisplay(DefaultDisplay)
	if err != nil {
		return nil, err
	}
	connection := display.Connection()
	system := &WaylandWindowSystem{
		display:    display,
		connection: connection,
		keyboard:   wayland.NewKeyboard(connection),
		pointer:    wayland.NewPointer(connection),
	}
	return system, nil
}

func (s *WaylandWindowSystem) PressKey(key uint32) {
	s.keyboard.KeyChan <- wayland.KeyboardKeyEvent{Key: key}
}

func (s *WaylandWindowSystem) MovePointer(from, to Point) {
	s.pointer.MotionChan <- wayland.PointerMotionEvent{
		SurfaceX: float32(to.X - from.X),
		SurfaceY: float32(to.Y - from.Y),
	}
}

func (s *WaylandWindowSystem) SetClipboard(raw string) {
	panic("Not yet implemented")
}
