// Copyright 2017, the project authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE.md file.

package cmd

import (
	"os"

	"github.com/op/go-logging"
)

// Initializes common logging backend.
func init() {
	const format = `%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.4s} %{id:03x}%{color:reset} %{message}`

	console := logging.NewLogBackend(os.Stdout, "", 0)
	logging.NewBackendFormatter(console, logging.MustStringFormatter(format))

	logging.SetBackend(console)
}
