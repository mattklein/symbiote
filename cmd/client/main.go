// Copyright 2017, the project authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE.md file.

package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/mattklein/symbiote"
	"bitbucket.org/mattklein/symbiote/networking"
	"bitbucket.org/mattklein/symbiote/platform"
)

var (
	host   = flag.String("host", "", "The host address of the remote server, or the address to bind to locally.")
	port   = flag.Int("port", 0, "The port on the remote server, or the port to bind to locally.")
	system = flag.String("system", "", "The type of window system to engage. If none specified, a default will be provided.")
)

// The main daemon service for the Symbiote client.
func main() {
	parseCommandLine()

	// setup posix signal handlers
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	// connect the network client
	client, err := networking.Connect(*host, *port)
	if err != nil {
		log.Fatal("Failed to connect: ", err)
	}
	defer client.Close()

	// start the symbiote driver
	system, err := platform.BuildSystem(*system)
	if err != nil {
		log.Fatal("Failed to start driver: ", err)
	}
	driver := symbiote.NewDriver(client, system)
	driver.Start()
	defer driver.Stop()

	// wait for an exit signal
	<-signals
	os.Exit(0)
}

// Parses and validates the command line flags and arguments.
func parseCommandLine() {
	flag.Parse()

	if *host == "" {
		flag.Usage()
		log.Fatal("A valid host must be provided")
	}

	if *port == 0 {
		flag.Usage()
		log.Fatal("A valid port must be provided.")
	}

	if *system == "" {
		*system = platform.SelectDefault()
	}

	if !platform.IsSupported(*system) {
		log.Fatal("The given system: ", system, " is not supported")
	}
}
