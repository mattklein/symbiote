// Copyright 2017, the project authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE.md file.

package symbiote

import (
	"bitbucket.org/mattklein/symbiote/networking"
	"bitbucket.org/mattklein/symbiote/networking/messages"
	"bitbucket.org/mattklein/symbiote/platform"
	"bitbucket.org/mattklein/symbiote/utils"
	"github.com/golang/protobuf/proto"
)

var log = logging.MustGetLogger("driver")

// A driver takes a platform window system and a client connection to a server
// and transforms messages from that client into interactions on the window system.
type Driver struct {
	Client    *networking.Client    // The underlying networking client.
	System    platform.WindowSystem // The underlying windowing system.
	isRunning utils.AtomicBool      // True if the driver is running, otherwise false.
}

// Creates a new driver for the given client and system.
func NewDriver(client *networking.Client, system platform.WindowSystem) *Driver {
	return &Driver{
		Client:    client,
		System:    system,
		isRunning: utils.AtomicFalse,
	}
}

// Starts the driver.
func (d *Driver) Start() {
	d.isRunning.SetTrue()
	go d.dispatch()
}

// Stops the driver
func (d *Driver) Stop() {
	d.isRunning.SetFalse()
}

// Dispatches instructions from the client to the instruction handlers.
func (d *Driver) dispatch() {
	for d.isRunning.IsTrue() {
		i := d.Client.Receive() // receive instructions from the network

		switch i.Type {
		case messages.Instruction_PRESS_KEY:
			msg := &messages.PressKey{}
			err := proto.Unmarshal(i.Data, msg)
			if err != nil {
				log.Errorf("Unmarshal PRESS_KEY failed: %s", err)
				continue
			}
			d.System.PressKey(msg.Keycode)

		case messages.Instruction_MOVE_POINTER:
			msg := &messages.MovePointer{}
			err := proto.Unmarshal(i.Data, msg)
			if err != nil {
				log.Errorf("Unmarshal MOVE_POINTER failed: %s", err)
				continue
			}
			d.System.MovePointer(
				platform.Point{msg.From.X, msg.From.Y},
				platform.Point{msg.To.X, msg.To.Y},
			)

		case messages.Instruction_SET_CLIPBOARD:
			msg := &messages.SetClipboard{}
			err := proto.Unmarshal(i.Data, msg)
			if err != nil {
				log.Errorf("Unmarshal SET_CLIPBOARD failed: %s", err)
				continue
			}
			d.System.SetClipboard(msg.Contents)
		}
	}
}
