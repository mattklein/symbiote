// Copyright 2017, the project authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE.md file.

package networking

import (
	"testing"
)

func TestServe(t *testing.T) {
	server, err := Serve("127.0.0.1", 7337)
	if err != nil {
		t.Error(err)
	}
	defer server.Close()
}
