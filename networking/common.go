// Copyright 2017, the project authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE.md file.

package networking

import "github.com/op/go-logging"

// Shared logger for networking components.
var log = logging.MustGetLogger("networking")
