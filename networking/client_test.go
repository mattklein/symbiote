// Copyright 2017, the project authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE.md file.

package networking

import "testing"

func TestConnect(t *testing.T) {
	const (
		TestHost = "127.0.0.1"
		TestPort = 7337
	)

	// start a server
	server, err := Serve(TestHost, TestPort)
	if err != nil {
		t.Error(err)
	}
	defer server.Close()

	// connect with the client
	client, err := Connect(TestHost, TestPort)
	if err != nil {
		t.Error(err)
	}
	defer client.Close()
}
