// Copyright 2017, the project authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE.md file.

package networking

import (
	"net"
	"strconv"

	"io"

	"bytes"

	"bitbucket.org/mattklein/symbiote/networking/messages"
	"bitbucket.org/mattklein/symbiote/utils"
	"github.com/golang/protobuf/proto"
)

// The Symbiote client, capable of handling high frequency network messaging for desktop interaction.
type Client struct {
	Host       string           // The host address of the server that the client is connected to.
	Port       int              // The the port that the client is connected to.
	connection net.Conn         // The underlying network connection.
	running    utils.AtomicBool // True if the server is running, otherwise false.

	incoming chan *messages.Instruction
	outgoing chan *messages.Event
}

// Connects to a server at the given host and port.
func Connect(host string, port int) (*Client, error) {
	connection, err := net.Dial("tcp", host+":"+strconv.Itoa(port))
	if err != nil {
		return nil, err
	}

	log.Infof("Connecting to %s:%d", host, port)

	client := &Client{
		Host:       host,
		Port:       port,
		connection: connection,
		running:    utils.AtomicTrue,
		incoming:   make(chan *messages.Instruction),
		outgoing:   make(chan *messages.Event),
	}

	go client.handleInbound()
	go client.handleOutbound()

	return client, nil
}

// Receives an instruction from the client.
// This is a blocking operation.
func (c *Client) Receive() *messages.Instruction {
	return <-c.incoming
}

// Sends an event to the server.
// This is a non-blocking operation.
func (c *Client) Send(event *messages.Event) {
	c.outgoing <- event
}

// Closes the client, dropping the remaining messages.
func (c *Client) Close() {
	close(c.incoming)
	close(c.outgoing)
	c.running.SetFalse()
	c.connection.Close()
}

func (c *Client) handleInbound() {
	var buffer bytes.Buffer

	for c.running.IsTrue() {
		// read data from the server into a buffer
		_, err := io.Copy(&buffer, c.connection)
		if err != nil {
			// end of stream? connection closed
			if err == io.EOF {
				log.Info("Disconnected from server.")
				break
			}
			log.Errorf("I/O error: %s", err)
			break
		}

		// unmarshal the instruction from the buffer
		instruction := &messages.Instruction{}
		err = proto.Unmarshal(buffer.Bytes(), instruction)
		if err != nil {
			log.Errorf("Unmarshal instruction failed: %s", err)
			continue
		}

		// push the instruction onto our channel for processing
		c.incoming <- instruction
	}
}

func (c *Client) handleOutbound() {
	for c.running.IsTrue() {
		// pull outgoing events from our channel
		event := <-c.outgoing

		// marshal the data into a buffer
		buffer, err := proto.Marshal(event)
		if err != nil {
			log.Errorf("Marshal event failed: %s", err)
			continue
		}

		// push the raw bytes to the server
		c.connection.Write(buffer)
	}
}
