// Copyright 2017, the project authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE.md file.

//go:generate protoc --go_out=. ./instructions.proto
//go:generate protoc --go_out=. ./events.proto
package messages
