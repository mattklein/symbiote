// Copyright 2017, the project authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE.md file.

package networking

import (
	"net"
	"strconv"

	"io"

	"bytes"

	"bitbucket.org/mattklein/symbiote/networking/messages"
	"bitbucket.org/mattklein/symbiote/utils"
	"github.com/golang/protobuf/proto"
)

// The Symbiote server, capable of handling high frequency network messaging for desktop interaction.
type Server struct {
	Host     string           // Host is the the host address the server is bound to.
	Port     int              // Port is the port that the server is listening on.
	listener net.Listener     // The underlying network listener.
	running  utils.AtomicBool // True if the server is running, otherwise false.

	incoming chan *messages.Event
	outgoing chan *messages.Instruction
}

// Starts a new server on the given host and port.
func Serve(host string, port int) (*Server, error) {
	listener, err := net.Listen("tcp", host+":"+strconv.Itoa(port))
	if err != nil {
		return nil, err
	}

	log.Infof("Listening on %s:%d", host, port)

	server := &Server{
		Host:     host,
		Port:     port,
		listener: listener,
		running:  utils.AtomicFalse,
		incoming: make(chan *messages.Event),
		outgoing: make(chan *messages.Instruction),
	}

	go server.listen()

	return server, nil
}

// Starts listening for client connections.
func (s *Server) listen() {
	s.running.SetTrue()

	for s.running.IsTrue() {
		connection, err := s.listener.Accept()
		if err != nil {
			// TODO: handle this better?
			break
		}

		log.Infof("Client %s connected", connection.RemoteAddr())

		go s.handleInbound(connection)
		go s.handleOutbound(connection)
	}
}

// Receives an event from the client.
// This is a blocking operation.
func (c *Server) Receive() *messages.Event {
	return <-c.incoming
}

// Dispatches an instruction to the connected clients.
// This is a non-blocking operation.
func (s *Server) Send(message *messages.Instruction) {
	s.outgoing <- message
}

// Stops the server, killing off any remaining connections.
func (s *Server) Close() {
	close(s.incoming)
	close(s.outgoing)
	s.running.SetFalse()
	s.listener.Close()
}

func (s *Server) handleInbound(connection net.Conn) {
	var buffer bytes.Buffer

	for s.running.IsTrue() {
		// read data from the client into a buffer
		_, err := io.Copy(&buffer, connection)
		if err != nil {
			// end of stream? connection closed
			if err == io.EOF {
				log.Infof("Client %s disconnected", connection.RemoteAddr())
				break
			}
			log.Errorf("I/O error: %s", err)
			break
		}

		// unmarshal events
		event := &messages.Event{}
		err = proto.Unmarshal(buffer.Bytes(), event)
		if err != nil {
			log.Errorf("Unmarshal event failed: %s", err)
			continue
		}

		// psuh for later processing
		s.incoming <- event
	}
}

func (s *Server) handleOutbound(connection net.Conn) {
	for s.running.IsTrue() {
		// TODO: this only works for a single client? how to make this multi-dispatch
		// pull outgoing instruction from our channel
		instruction := <-s.outgoing

		// marshal the data into a buffer
		buffer, err := proto.Marshal(instruction)
		if err != nil {
			log.Errorf("Marshal instruction failed: %s", err)
			continue
		}

		// push the raw bytes to the client
		connection.Write(buffer)
	}
}
